/*
 * Hi-Tech C I2C library for 16F1825
 * Master mode routines for I2C MSSP port to read and write to slave device 
 * Copyright (C)2011 HobbyTronics.co.uk 2011
 * Freely distributable.
*/

#define I2C_WRITE 0
#define I2C_READ 1
#define _XTAL_FREQ 4000000.0    /*for 4mhz*/

void i2c_Init_Slave(unsigned char Address){
    TRISC4=1;           	// set SCL and SDA pins as inputs
	TRISC3=1;
}

// Initialise MSSP1 port. (16F1829 on VIVA board - other devices may differ)
void i2c_Init(void){
	// Initialise I2C MSSP 1 module
	// Master 100KHz
	TRISC4=1;           	// set SCL and SDA pins as inputs
	TRISC3=1;

	SSPCON1 = 0b00101000; 	// I2C enabled, Master mode
             //-0------	SSPOV=receive overflow indicator (read data in 
             //			sspbuf before new data comes in to prevent error.
             //			Check and clear to ensure sspbuf can be updated
             //--1-----	SSPEN=1 (Enable I2C serial port)
             //----1000	1000 = I2C Master Mode. 
             //			Clock=FOSC/(4*(SSPADD+1))
    // I2C Master mode, clock = FOSC/(4 * (SSP1ADD + 1)) 
    SSPADD = 3;    		// 100Khz @ 4Mhz Fosc  use 9 to 3
                        //SCL pin clock period=FOSC/(4*(SSPADD+1))
                        //SSPADD=255.
                        //Baud=(4*10^6) / (4*(9+1)) = 100000 bps

	SSPSTAT = 0b11000000; 	// Slew rate disabled
    SSPCON2 = 0;

}

// i2c_Wait - wait for I2C transfer to finish
void i2c_Wait(void){
    //while ( ( SSP1CON2 & 0x1F ) || ( SSP1STAT & 0x04 ) ); 
    __delay_ms(1);
}

// i2c_Start - Start I2C communication
void i2c_Start(void)
{
 	i2c_Wait();
	SSPCON2bits.SEN=1;
}

// i2c_Restart - Re-Start I2C communication
void i2c_Restart(void){
 	i2c_Wait();
	SSPCON2bits.RSEN=1;
}

// i2c_Stop - Stop I2C communication
void i2c_Stop(void)
{
 	i2c_Wait();
 	SSPCON2bits.PEN=1;
}

// i2c_Write - Sends one byte of data
void i2c_Write(unsigned char data)
{
 	i2c_Wait();
 	SSPBUF = data;
}

// i2c_Address - Sends Slave Address and Read/Write mode
// mode is either I2C_WRITE or I2C_READ
void i2c_Address(unsigned char address, unsigned char mode)
{
	unsigned char l_address;

	l_address=address<<1;
	l_address+=mode;
 	i2c_Wait();
 	SSPBUF = l_address;
}

// i2c_Read - Reads a byte from Slave device
unsigned char i2c_Read(unsigned char ack)
{
	// Read data from slave
	// ack should be 1 if there is going to be more data read
	// ack should be 0 if this is the last byte of data read
 	unsigned char i2cReadData;

 	i2c_Wait();
	SSPCON2bits.RCEN=1;
 	i2c_Wait();
 	i2cReadData = SSPBUF;
 	i2c_Wait();
 	if ( ack ) SSPCON2bits.ACKDT=0;			// Ack
	else       SSPCON2bits.ACKDT=1;			// NAck
	SSPCON2bits.ACKEN=1;   		            // send acknowledge sequence

	return( i2cReadData );
}

