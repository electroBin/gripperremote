EE-3463 (Micro 1) Final Project.
Remote Controller for Gripper Arm.

Program to control a gripper arm consisting of one servo motor and 2 stepper
motors. Commands are received from the master controller via UART/RS485.  Servo 
motor is driven via PWM and the two steppers are driven by an A4988 driver each. 
The microstepping configuration of the A4988 drivers is displayed on the 2x16 
character display via i2c

Daniel Lawton
(Group 25)

