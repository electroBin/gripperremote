/*Program to control a gripper arm consisting of one servo motor and 2 stepper
 motors. Commands are received from the master controller via UART/RS485.  Servo 
 motor is driven via PWM and the two steppers are driven by an A4988 driver each. 
 The microstepping configuration of the A4988 drivers is displayed on the 2x16 
 character display via i2c*/
#include  "remote_init.h"

//Prototypes

void uart_TX(unsigned int);
unsigned int uart_RX(void);


extern void PWM_init(void);
extern void initialize(void);

//Global Variables
unsigned int transData = 0;
unsigned int receiveData = 0;
unsigned int oldReceiveData = 0;
unsigned int servoDuty;
unsigned int packet = 0;
unsigned char  Sout[16];
unsigned char * Sptr;

void interrupt ISR(void){
    packet = uart_RX();
    
    if(packet != 1 && packet != 2 && packet != 4 && packet != 5){
        CCPR2L = packet; //Packet contains servo PWM data
    }

    RCIF = 0;       //Clear UART receive interrupt flag

}

void main(void) {
    initialize();

    while(1) {
        while(packet == 1){ //Tilting up
            LATD7 = 0;
            LATE0 ^= 1;
            __delay_us(750);
        }
        while(packet == 2){ //Tilting down
            LATD7 = 1;
            LATE0 ^= 1;
            __delay_us(750);
        }
        while(packet == 4){ //Rotate clockwise
            LATD6 = 1;
            LATC2 ^= 1;
            __delay_us(500);
        }
        while(packet == 5){ //Rotate counter-clockwise
            LATD6 = 0;
            LATC2 ^= 1;
            __delay_us(500);
        }
    }
}






