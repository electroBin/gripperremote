// PIC16F1937 Configuration Bit Settings
// 'C' source line config statements

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Memory Code Protection (Data memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = OFF       // Internal/External Switchover (Internal/External Switchover mode is disabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is disabled)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config PLLEN = OFF      // PLL Enable (4x PLL disabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LVP = ON        // Low-Voltage Programming Enable (High-voltage on MCLR/VPP must be used for programming)


#define I2C_SLAVE 0x27	/* was 1E Channel of i2c slave depends on soldering on back of board*/
#define _XTAL_FREQ 4000000.0    /*for 4mhz*/
/*Serial Configuration*/
#define BAUD 57600   //Bits per second transfer rate
#define FOSC 4000000L   //Frequency Oscillator
#define DIVIDER ((int)(FOSC/(16UL * BAUD) -1))  //Should be 25 for 9600/4MhZ
#define TX_enable RA0

#include <xc.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include  "i2c.h"
#include  "i2c_LCD.h"

#define TX_enable RA0
#define _XTAL_FREQ 4000000.0    /*for 4mhz*/

extern unsigned int transData;
extern unsigned int receiveData;
extern void uart_TX(unsigned int);
extern unsigned int uart_RX(void);

void clockAndpin_config(){
    TRISA = 0b11111100;
            //------0-      RA1 = Signal line going to master
            //-----1--      RA2 = Signal line coming from master
            //-------0      TX enable for RS485
    
    TRISB = 0b11101110;
            //1-------      ICSPDAT
            //-1------      ICSPCLK
            //--1-----      PORTB, 5 = MS1 (big stepper)
            //---0----      PORTB, 4 = Enable (big stepper)
            //----1---      PORTB, 3 = MS3 (small stepper)
            //-----1--      PORTB, 2 = MS2 (small stepper)
            //------1-      PORTB, 1 = MS1 (small stepper)
            //-------0      PORTB, 0 = Enable (small stepper)
    
    TRISC = 0b11111001;
            //1-------      PORTC, 7 = UART RX
            //-1------      PORTC, 6 = UART TX
            //---1----      PORTC, 4 = I2C SDA
            //----1---      PORTC, 3 = I2C SCL
            //-----0--      PORTC, 2 = Step pin (large stepper)
            //------0-      PORTC, 1 = PWM servo
    
    TRISD = 0b00111111;
            //0-------      PORTD, 7 = Stepper direction (small stepper)
            //-0------      PORTD, 6 = Stepper direction (large stepper)
            //--1-----      PORTD, 5 = MS3 (big stepper)
            //---1----      PORTD, 4 = MS2 (big stepper)
            //----1---      PORTD, 3 = tilt hall-effect sensor
            //-----1--      PORTD, 2 = rotation hall-effet sensor
            
    TRISE = 0b11111110;
            //-------0      PORTE, 0 = Step pin (small stepper)
    
    ANSELA = 0;
    ANSELB = 0;
    ANSELD = 0;
    ANSELE = 0;
    
    INTCON = 0b01000000;
    
    RA1 = 0;
    __delay_ms(250);
}

void PWM_init(void){
    /*********************Configure PWM****************************************/
        T2CON = 0b00000111;
                //-----1--	enable TMR2
                //------11	prescale=64 (T2CKPS=11)
                //          TMR2 increments every 64 uS
        
        PR2 = 255;      //255*64uS = 16.3mS period = 61Hz
    
        CCPTMRS0 = 0b00000000;       
                   //----00--	;CCP2 based off of TMR2
                                //P2A = PORTC, 1
    
        CCP2CON = 0b00001100;
                  //00------  Single output mode (P2A modulated, P2B, P2C, P2D assigned as port pins
                  //--00----  DC2B=00 (2 LSB's of PWM duty cycle=00)
                  //----1100  PWM mode (all active-high) CCP2M=1100

        //Enable both steppers
        RB4 = 0;    //Big stepper enabled
        RB0 = 0;    //Small stepper enabled

        return;
    }

void startup(void){
    //Pull RA1 high so master can proceed
    RA1 = 1;
    
}

void setup_comms(void){
    SPBRG = DIVIDER;
    RCSTA = 0b10010000;
            //1-------  :Serial port enabled (SPEN = 1)
            //-0------  :8-bit reception (RX9 = 0)
            //---1----  :Enable receiver (CREN = 1)
            //----0---  :Disable address detection (ADDEN = 0)
                       //all bytes are received and 9th bit can be used as
                       //parity bit 
    
    TXSTA = 0b00100100;
            //-0------  :8-bit transmission (TX9 = 0)
            //--1-----  :Enable transmission (TXEN = 1)
            //---0----  :Asynchronous mode (SYNC = 0)
            //-----0--  :Low speed baud rate (BRGH = 0)
}