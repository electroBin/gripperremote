#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

#define _XTAL_FREQ 4000000.0    /*for 4mhz*/
#define I2C_SLAVE 0x27	/* was 1E Channel of i2c slave depends on soldering on back of board*/
#define TX_enable RA0

extern void setup_comms(void);
extern unsigned char * Sptr;
extern void clockAndpin_config(void);
extern void i2c_Init(void);
extern void PWM_init(void);
extern void startup(void);
void I2C_LCD_Command(unsigned char,unsigned char);
void I2C_LCD_SWrite(unsigned char,unsigned char *, char);
void I2C_LCD_Init(unsigned char);
void I2C_LCD_Pos(unsigned char,unsigned char);
unsigned char I2C_LCD_Busy(unsigned char);

void uart_TX(unsigned int txData){
    TXREG = txData;
    __delay_ms(1);
    while(TXIF == 0){   //Is TX buffer full? (1=empty, 0=full)
        continue;
    }
}

unsigned int uart_RX(void){
    unsigned int dataReceived = 0;
    while(RCIF == 0){   //Is RX buffer full? (1=full, 0=notfull)
        continue;
    }
    //RCSTA |= (1 << 5);      //Disable receiver
    dataReceived = RCREG;   //Save data received
    //PIR1 |= (1 << 5);       //Clear UART receive interrupt flag
    //RCSTA &= ~(1 << 5);      //Enable receiver
    
    return dataReceived;
}

void initialize(void){
    extern unsigned char  Sout[16];
    Sptr = Sout;
    OSCCON  = 0x6A; /* b6..4 = 1101 = 4MHz */
     /*  ********************************I2C**************************** */
    /*  Note the I2C write to LCD uses the 8 bits of the PCF8574 chip to control the LCD  */
    /*  Connected High 4 bits are High 4 data for LCD (use in 4 bit mode)  the other 4 are     */
    /*  bit3=turn on /off bk light  bit 2= E line writes on Hi 2 Lo transition, reads Lo to Hi */
    /*  bit 2=Read write set  to 0 for write   bit 0=RS  command=0 Data=1      */
    /*  ********************************I2C**************************** */
    
    clockAndpin_config();
    setup_comms();
    i2c_Init();				// Start I2C as Master 100KH
    
    I2C_LCD_Init(I2C_SLAVE); //pass I2C_SLAVE to the init function to create an instance
    I2C_LCD_Command(I2C_SLAVE, 0b00001100); //Display on, underline off, blink off
    I2C_LCD_Pos(I2C_SLAVE, 0x0); //Set Position 
    
    I2C_LCD_Command(I2C_SLAVE, 0x01); //Clear display
    sprintf(Sout, "Initializing");
    I2C_LCD_SWrite(I2C_SLAVE, Sout, 12);
    
    PWM_init();
    
    /*Display headers for stepper configuration*/
    //ROTATION
    I2C_LCD_Command(I2C_SLAVE, 0x01); //Clear display
    sprintf(Sout, "Rot: ");
    I2C_LCD_SWrite(I2C_SLAVE, Sout, 5);
    
    if(RB5 == 0 && RD4 == 0 && RD5 == 0){
        I2C_LCD_Pos(I2C_SLAVE, 0x05); //Set Position
        sprintf(Sout, "Full step");
        I2C_LCD_SWrite(I2C_SLAVE, Sout, 9);
    }
    else if(RB5 == 1 && RD4 == 0 && RD5 == 0){
        I2C_LCD_Pos(I2C_SLAVE, 0x05); //Set Position
        sprintf(Sout, "1/2 step ");
        I2C_LCD_SWrite(I2C_SLAVE, Sout, 9);
    }
    else if(RB5 == 0 && RD4 == 1 && RD5 == 0){
        I2C_LCD_Pos(I2C_SLAVE, 0x05); //Set Position
        sprintf(Sout, "1/4 step ");
        I2C_LCD_SWrite(I2C_SLAVE, Sout, 9);
    }
    else if(RB5 == 1 && RD4 == 1 && RD5 == 0){
        I2C_LCD_Pos(I2C_SLAVE, 0x05); //Set Position
        sprintf(Sout, "1/8 step ");
        I2C_LCD_SWrite(I2C_SLAVE, Sout, 9);
    }
    else{
        I2C_LCD_Pos(I2C_SLAVE, 0x05); //Set Position
        sprintf(Sout, "1/16 step");
        I2C_LCD_SWrite(I2C_SLAVE, Sout, 9);
    }
    
    //TILT
    I2C_LCD_Pos(I2C_SLAVE, 0x40); //Set Position
    sprintf(Sout, "Tilt: ");
    I2C_LCD_SWrite(I2C_SLAVE, Sout, 6);
    
    if(RB1 == 0 && RB2 == 0 && RB3 == 0){
        I2C_LCD_Pos(I2C_SLAVE, 0x46); //Set Position
        sprintf(Sout, "Full step");
        I2C_LCD_SWrite(I2C_SLAVE, Sout, 9);
    }
    else if(RB1 == 1 && RB2 == 0 && RB3 == 0){
        I2C_LCD_Pos(I2C_SLAVE, 0x46); //Set Position
        sprintf(Sout, "1/2 step ");
        I2C_LCD_SWrite(I2C_SLAVE, Sout, 9);
    }
    else if(RB1 == 0 && RB2 == 1 && RB3 == 0){
        I2C_LCD_Pos(I2C_SLAVE, 0x46); //Set Position
        sprintf(Sout, "1/4 step ");
        I2C_LCD_SWrite(I2C_SLAVE, Sout, 9);
    }
    else if(RB1 == 1 && RB2 == 1 && RB3 == 0){
        I2C_LCD_Pos(I2C_SLAVE, 0x46); //Set Position
        sprintf(Sout, "1/8 step ");
        I2C_LCD_SWrite(I2C_SLAVE, Sout, 9);
    }
    else{
        I2C_LCD_Pos(I2C_SLAVE, 0x46); //Set Position
        sprintf(Sout, "1/16 step");
        I2C_LCD_SWrite(I2C_SLAVE, Sout, 9);
    }
    
    startup();   

    __delay_ms(1000);
    /********************Configure Interrupts**********************************/
    INTCON = 0b11000000;
//             1-------	;Enable global interrupts (GIE=1)
//             -1------	;Enable peripheral interrupts (PEIE=1)
//             --0-----	;Disable TMR0 interrupts (TMROIE=0)
//	       ---0----	;Disable RBO/INT external interrupt (INTE=1)
//             ----0---	;Disable interrupt on change for PORTB (IOCIE=0)
    
    //PIE1bits.RCIE=1;      
    //PIE1 &= ~(1 << 5);      //Enable UART Receive interrupts
    RCIE = 1;
    
    TX_enable = 0;
}